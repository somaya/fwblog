@extends("admin.layouts.app")
@section('title')
    Dashbord
@endsection
@section('content')
    <div class="page-wrapper">

        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        {{--<h3 class="page-title">اهلا!</h3>--}}
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active">Admin Panel</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$users}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/users"><h6 class="text-muted">Users</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$users}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$admins}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/admins"><h6 class="text-muted">Admins</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$admins}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$categories}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/categories"><h6 class="text-muted">Categories</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$categories}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$posts}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/posts"><h6 class="text-muted">Posts</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$posts}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
@endsection

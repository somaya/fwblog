
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label"> First Name</label>
    <div class="col-lg-5{{ $errors->has('f_name') ? ' has-danger' : '' }}">
        {!! Form::text('f_name',old('f_name'),['class'=>'form-control m-input','autofocus','placeholder'=> 'First Name' ]) !!}
        @if ($errors->has('f_name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('f_name ') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">Last Name</label>
    <div class="col-lg-5{{ $errors->has('l_name') ? ' has-danger' : '' }}">
        {!! Form::text('l_name',old('l_name'),['class'=>'form-control m-input','placeholder'=> 'Last Name' ]) !!}
        @if ($errors->has('l_name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('l_name') }}</strong>
            </span>
        @endif
    </div>




</div>


<div class="form-group m-form__group row">

    <label class="col-lg-1 col-form-label">Email</label>
    <div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('email',old('email'),['class'=>'form-control m-input','placeholder'=> 'Email' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">Password</label>
    <div class="col-lg-5{{ $errors->has('password') ? ' has-danger' : '' }}">
        {!! Form::password('password',['class'=>'form-control m-input','placeholder'=> 'Password' ]) !!}
        @if ($errors->has('password'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">Category</label>
    <div class="col-lg-10{{ $errors->has('category_id') ? ' has-danger' : '' }}">
        {{--{!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'Phone' ]) !!}--}}
        <select name="category_id" id="category_id" class="form-control m-input" >
            <option value="">--Catgory--</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}" {{$user ?? '' && $user->category_id==$category->id ?'selected':''}}>{{$category->name}}</option>
            @endforeach

        </select>
        @if ($errors->has('category_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>


</div>



<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">Photo</label>
    <div class="col-lg-11{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if($user ?? '' && $user->photo)
<div class="row">




            <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                <img
                        data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                        alt="First slide [800x4a00]"
                        src="{{asset($user->photo)}}"
                        style="height: 150px; width: 150px"
                        data-holder-rendered="true">
            </div>

</div>
@endif



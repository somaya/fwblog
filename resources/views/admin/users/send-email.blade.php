@extends('admin.layouts.app')

@section('title')
    Send Email
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/users')}}" class="m-menu__link">
            <span class="m-menu__link-text">Users</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">Send Email</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                       Send Email
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right" action="/webadmin/store-email" method="post" enctype="multipart/form-data" >
            <div class="m-portlet__body">
                @csrf
                <div class="form-group m-form__group row">
                    <label class="col-lg-2 col-form-label">User : </label>
                    <div class="col-lg-10{{ $errors->has('user_id') ? ' has-danger' : '' }}">
                        <select name="user_id[]" data-placeholder="User" class="form-control  " multiple >
                            <option >select users</option>
                            <option value="all" >All Users</option>

                                @foreach($users as $user)
                                    <option value="{{$user->id}}" >{{$user->email}} </option>
                                @endforeach



                        </select>
                        @if ($errors->has('user_id'))
                            <span class="form-control-feedback" role="alert">
                                <strong>{{ $errors->first('user_id') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
                <div class="form-group m-form__group row">
                    <label class="col-lg-2 col-form-label">Message : </label>
                    <div class="col-lg-10{{ $errors->has('message') ? ' has-danger' : '' }}">
                        <textarea class="form-control m-input" name="message" value="" ></textarea>
                        @if ($errors->has('message'))
                            <span class="form-control-feedback" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>


            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection


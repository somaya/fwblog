


{{--@if(count($errors->all())>0)--}}
    {{--@push('js')--}}
        {{--<script src="{{asset('website/js/bootstrap-notify/bootstrap-notify.js')}}"></script>--}}
        {{--@foreach($errors->all() as $error)--}}

            {{--<script>--}}

                {{--$.notify({--}}
{{--// options--}}
                    {{--icon: 'fa fa-times',--}}
                    {{--title: '',--}}
                    {{--message: '{{$error}}',--}}
                {{--}, {--}}
{{--// settings--}}
                    {{--element: 'body',--}}
                    {{--position: null,--}}
                    {{--type: "danger",--}}
                    {{--allow_dismiss: false,--}}
                    {{--newest_on_top: true,--}}
                    {{--showProgressbar: false,--}}
                    {{--placement: {--}}
                        {{--from: "bottom",--}}
                        {{--align: "left"--}}
                    {{--},--}}
                    {{--offset: 20,--}}
                    {{--spacing: 10,--}}
                    {{--z_index: 2000,--}}
                    {{--delay: 8000,--}}
                    {{--timer: 1500,--}}
                    {{--url_target: '_blank',--}}
                    {{--animate: {--}}
                        {{--enter: 'animated fadeInDown',--}}
                        {{--exit: 'animated fadeOutUp'--}}
                    {{--},--}}

                {{--});--}}

            {{--</script>--}}

        {{--@endforeach--}}

    {{--@endpush--}}




{{--@endif--}}
@if(session()->has('success'))

    @push('js')
        <script src="{{asset('website/js/bootstrap-notify/bootstrap-notify.js')}}"></script>


        {{--<script src="{{asset('admin/')}}/js/pages/ui/notifications.js"></script>--}}
        <script>

            $.notify({
// options
                icon: 'fa fa-check',
                title: '',
                message: '{{session('success')}}',
            }, {
// settings
                element: 'body',
                position: null,
                type: "success",
                allow_dismiss: false,
                newest_on_top: true,
                showProgressbar: false,
                placement: {
                    from: "bottom",
                    align: "left"
                },
                offset: 20,
                spacing: 10,
                z_index: 2000,
                delay: 5000,
                timer: 1000,
                url_target: '_blank',
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },

            });

        </script>


    @endpush



@endif
@if(session()->has('error'))

    @push('js')
        <script src="{{asset('website/js/bootstrap-notify/bootstrap-notify.js')}}"></script>


        {{--<script src="{{asset('admin/')}}/js/pages/ui/notifications.js"></script>--}}
        <script>

            $.notify({
// options
                icon: 'fa fa-times',
                title: '',
                message: '{{session('error')}}',
            }, {
// settings
                element: 'body',
                position: null,
                type: "danger",
                allow_dismiss: false,
                newest_on_top: true,
                showProgressbar: false,
                placement: {
                    from: "bottom",
                    align: "left"
                },
                offset: 20,
                spacing: 10,
                z_index: 2000,
                delay: 8000,
                timer: 1500,
                url_target: '_blank',
                animate: {
                    enter: 'animated fadeInDown',
                    exit: 'animated fadeOutUp'
                },

            });

        </script>


    @endpush


@endif



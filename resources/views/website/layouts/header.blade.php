<div class="overlay-citys"></div>
        <link rel="stylesheet" href="{{asset('/website/css/nav.css')}}">
<style>

</style>
<div class="wrap">

    <div class="upper-bar">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-4 align-self-center">
                    <a href="/posts" class="logo-link"><img src="/website/img/logo-h.png" width="120px"/></a>


                </div>
                <div class="col-md-8 align-self-center hide-in-sm" dir="ltr">
                    {{--@if(auth()->check())--}}
                        {{--<ul class="list-unstyled d-flex align-items-center justify-content-end u-links">--}}
                            {{--<li class="_nav-item"><a href="/favourites"> <i class="fa fa-heart"></i>--}}
                                    {{--<span> المفضلة </span> </a></li>--}}
                            {{--<li class="_nav-item"><a href="/chats"> <i class="fa fa-comments"></i>--}}
                                    {{--<span> محادثاتي </span> </a></li>--}}
                        {{--</ul>--}}
                    {{--@endif--}}

                    {{--<a class="clk-cit"><i class="fa fa-globe fa-2x"></i> <i class="fa fa-angle-down fa-2x"></i></a>--}}
                    {{--@if(auth()->check())--}}
                        {{--<a class="login-header btn-add-header" href="{{url('add-ad?country='.session('country_id'))}}">اضف اعلانك</a>--}}
                    {{--@endif--}}
                    @if(!auth()->check())
                        <a href="/login" class="login-header"><span>Login</span> </a>
                    @else
                        <div style="display: inline-flex;">
                            <span data-toggle="dropdown">
                                    <h4>{{auth()->user()->f_name}} {{auth()->user()->l_name}}  <i class="fa fa-caret-down" aria-hidden="true"></i></h4>
                            </span>
                            <ul class="dropdown-menu" style="right: unset">
                                <li><a href="/profile" data-original-title="" title=""> Profile<i class="fa fa-user" aria-hidden="true"></i> </a></li>

                                @if(auth()->user()->role == 1)
                                    <li><a href="/webadmin/dashboard" data-original-title="" title="">Admin Panel <i class="fa fa-users" aria-hidden="true"></i> </a></li>
                                @endif
                                <li><a href="/logout" data-original-title="" title=""> Logout <i class="fa fa-power-off" aria-hidden="true"></i> </a></li>
                            </ul>
                        </div>
                    @endif

                    {{--<div class="city-select row">--}}
                        {{--<div class="box-city form-control" style="width: 26%; border: none ;display: none">--}}
                            {{--<select name="country_id" id="header_country_id" class="change_country">--}}
                                {{--<option>اختر الدوله</option>--}}
                                {{--@foreach(\App\Models\Country::get() as $country)--}}
                                    {{--<option--}}
                                        {{--value="{{$country->id}}" {{session('country_id') && session('country_id')== $country->id ?'selected':''}}>{{$country->name_ar}}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                            {{--<select name="city_id" id="header_city_id" class="change_city">--}}
                                {{--<option value="">اختر المدينه</option>--}}
                                {{--@if(session('country_id'))--}}
                                    {{--@foreach(\App\Models\Country::find(session('country_id'))->cities as $city)--}}
                                        {{--<option--}}
                                            {{--value="{{$city->id}}" {{session('city_id') && session('city_id')== $city->id ?'selected':''}}>{{$city->name_ar}}</option>--}}
                                    {{--@endforeach--}}
                                {{--@endif--}}

                            {{--</select>--}}


                        {{--</div>--}}

                    {{--</div>--}}


                </div>
            </div>
        </div>
    </div>


    <header id="header" class="hide-in-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {{--<div class="header-category-menu col-md-12">--}}





                        {{--<ul class="dubizzle_menu" data-ui-id="dubizzle_menu" data-header-id="list">--}}
                            {{--@if(isset($categories))--}}
                            {{--@foreach($categories as $category)--}}
                                  {{--@if($category->children)--}}
                                    {{--<li class="dubizzle_menu_item ">--}}
                                    {{--<a href="javascript:;" class="dubizzle_menu_item_link header_link" data-tr-event-name="click_vertical_selected" data-tr-context="{'cL1Id': 7, 'cL2Id': '', 'cL3Id': '', 'cL4Id': '', 'categoryId': 7}" data-ui-id="menu-MT">--}}
                                        {{--{{$category->name_ar}}--}}
                                    {{--</a>--}}

                                    {{--<div class="dubizzle_menu_dropdown dubizzle_menu_dropdown_MT">--}}
                                        {{--<div class="box-flex">--}}
                                            {{--<ul class="dubizzle_menu_dropdown_col dubizzle_menu_dropdown_col_childrens">--}}

                                                {{--@if($category->children)--}}

                                                    {{--@foreach($category->children as $child)--}}

                                                        {{--<li class="dubizzle_menu_dropdown_item " data-submenu-id="menu-{{$child->id}}">--}}
                                                            {{--<a href="@if(count($child->children??[])>0) javascript:;@else{{url('categories/'.$child->id)}}  @endif" onmouseover="menueUpdate('{{$child->id}}')" class="dubizzle_menu_dropdown_item_link header_link " title="{{$child->name_ar}}" data-tr-event-name="click_category_selected" data-tr-context="{'cL1Id': 7, 'cL2Id': 140, 'cL3Id': '', 'cL4Id': '', 'categoryId': {{$child->id}} }">--}}
                                                                {{--{{$child->name_ar}}--}}
                                                            {{--</a>--}}
                                                            {{--@if(count($child->children??[])>0)--}}
                                                            {{--<div id="menu-{{$child->id}}" class="children_dropdown" style="display: none;">--}}
                                                                {{--<header class="children_dropdown_header">--}}
                                                                    {{--<span class="children_dropdown_header_title">{{$child->name_ar}}</span>--}}
                                                                    {{--<a href="{{url('categories/'.$child->id)}}" data-tr-event-name="all_in_category_selected" class="children_dropdown_link">--}}
                                                                        {{--عرض  جميع الاعلانات--}}
                                                                    {{--</a>--}}
                                                                {{--</header>--}}
                                                                {{--<ul class="children_dropdown_list children_dropdown_list_premium">--}}
                                                                    {{--@foreach($child->children as $sub_child)--}}
                                                                        {{--<li class="children_dropdown_list_item">--}}
                                                                            {{--<a href="{{url('categories/'.$sub_child->id)}}" title=" {{$sub_child->name_ar}}" data-tr-event-name="click_cat3_selected"> {{$sub_child->name_ar}}</a>--}}
                                                                        {{--</li>--}}
                                                                    {{--@endforeach--}}
                                                                {{--</ul>--}}
                                                            {{--</div>--}}
                                                            {{--@endif--}}
                                                        {{--</li>--}}
                                                    {{--@endforeach--}}
                                                {{--@endif--}}






                                            {{--</ul>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</li>--}}


                                  {{--@endif--}}
                            {{--@endforeach--}}
                            {{--@endif--}}
                        {{--</ul>--}}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </header>
</div>
<script>
    function menueUpdate(id){
        $('.children_dropdown').hide(200);
        $('#menu-'+id).show(200);
    }
</script>

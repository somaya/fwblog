@extends("website.layouts.app")
@section('content')
    <div class="container">
        <div class="main-body">

            <!-- Breadcrumb -->
            <nav aria-label="breadcrumb" class="main-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/posts">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Profile</li>
                </ol>
            </nav>
            <!-- /Breadcrumb -->

            <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center">
                                <img src="{{$user->photo?:'https://bootdey.com/img/Content/avatar/avatar7.png'}}" alt="Admin" class="rounded-circle" width="150">
                                <div class="mt-3">
                                    <h4>{{$user->f_name}} {{$user->l_name}}</h4>
                                    {{--<p class="text-secondary mb-1">اخر ظهور قبل 7 دقيقة</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-3">

                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">Profile</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    {{$user->email}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5">
                                    <h5 class="mb-0">Category </h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    {{$user->category->name}}
                                </div>
                            </div>
                            <hr>


                        </div>

                    </div>
                </div>
                <div class="col-md-8">
                    @if($rates->isNotEmpty())
                    <section class="popular-places" id="popular">
                        <div class="">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-heading">
                                        {{--<span>اكسبريس ديليفري سيرفس</span>--}}
                                        <h2>Reviews</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="" dir="ltr">
                                @foreach($rates as $rate)
                                <div class="col-md-4 col-xs-6 popular-item">
                                    <div class="thumb">
                                        {{--<a href="/product/{{$product->id}}"><img src="{{count($product->ProductImage) > 0?$product->ProductImage[0]->image:''}}" alt=""></a>--}}
                                        <div class="text-content">
                                            <h4> Review :{{$rate->degree}}</h4>

                                        </div>
                                        <div class="text-content">
                                            <h4>Comment :{{$rate->comment}}</h4>

                                        </div>
                                        <div class="text-content">
                                            <h4>Time : {{$rate->created_at}}</h4>

                                        </div>
                                        <div class="text-content">
                                            <h4> Reviewed By : {{$rate->admin->f_name}} {{$rate->admin->l_name}}</h4>

                                        </div>


                                    </div>
                                </div>
                                @endforeach


                            </div>
                        </div>
                    </section>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
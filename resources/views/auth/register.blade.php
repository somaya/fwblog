@extends("website.layouts.app")
@section('content')
    <div class="login-page">
        <div class="container">
            <div class="col-md-6">

                <h3>Register</h3>

                <div class="login-form">
                    <form action="{{route('register')}}" method="post">
                        @csrf

                        <div class="form-g">
                            <input type="text" name="f_name" value="{{old('f_name')}}" placeholder="First Name">
                            @if ($errors->has('f_name'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('f_name') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="l_name" value="{{old('l_name')}}" placeholder="Last Name">
                            @if ($errors->has('l_name'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('l_name') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <input type="email" name="email" value="{{old('email')}}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('email') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password"  placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password_confirmation"  placeholder="Confirm Password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password_confirmation') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <select name="category_id" id="category_id" class="form-control" placeholder="Category">
                                <option value="">Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}"> {{$category->name}}</option>

                                @endforeach

                            </select>
                            @if ($errors->has('category_id'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('category_id') }}
                                   </strong>
                                </span>
                            @endif
                        </div><br>


                        <div class="form-g f-btn">
                            <button type="submit">Register</button>
                        </div>
                    </form>
                </div>
            </div>


            {{--<div class="col-md-6">--}}

                {{--<h3>التسجيل بالموقع</h3>--}}

                {{--<div class="login-form register-text">--}}
                    {{--للتسجيل بالموقع، يجب اولا الموافقة على اتفاقية الاستخدام:--}}

                    {{--إن اتفاقية الاستخدام هذه وخصوصية الاستخدام ، والشروط والبنود ، وجميع السياسات التي تم نشرها على مؤسسة موقع حراج للتسويق الإلكتروني وضعت لحماية وحفظ حقوق كل من ( مؤسسة موقع حراج للتسويق الإلكتروني ) و ( المستخدم الذي يصل إلى الموقع بتسجيل او من دون تسجيل )أو ( العميل المستفيد من الإعلانات بتسجيل أو من دون تسجيل).--}}

                    {{--تم إنشاء الاتفاقية بناء على نظام التعاملات الإلكترونية. تخضع البنود والشروط والأحكام والمنازعات القانونية للقوانين والتشريعات والأنظمة المعمول بها.--}}

                    {{--لكونك مستخدم فأنك توافق على الالتزام بكل ما يرد بهذه الاتفاقية في حال استخدامك للموقع او في حال الوصول اليه او في حالة التسجيل في الخدمة. يحق لموقع حراج التعديل على هذه الاتفاقية في أي وقت وتعتبر ملزمة لجميع الأطراف بعد الإعلان عن التحديث في الموقع أو في أي وسيلة آخرى.--}}
                {{--</div>--}}
            {{--</div>--}}


        </div>
    </div>

@endsection



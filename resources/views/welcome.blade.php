@extends('website.layouts.app')

@section('content')

    <div class="login-page">
        <div class="container">
            <div class="col-md-8 col-md-offset-2 pull-left col-sm-12 col-xs-12">

                <h3>Login</h3>
                <div class="col-md-4 align-self-center">
                   <img src="/website/img/logo-v.png" width="120px"/>


                </div>
                <div class="col-md-4 align-self-center">
                    <img src="/website/img/slogan.png" width="120px"/>


                </div>
                <br>

                <div class="login-form">
                    @include('message')
                    <form action="{{route('login')}}" method="post">
                        @csrf
                        <div class="form-g">
                            <input type="text" name="email" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('email') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <input type="password" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        {{--<div class="form-g">--}}
                        {{--<input type="checkbox">--}}
                        {{--<span>تذكرني</span>--}}
                        {{--</div>--}}

                        <div class="form-g f-btn">
                            <button type="submit">Login</button>
                        </div>
                    </form>

                    <div class="register-btn f-btn">
                        <a href="/register">Register</a>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection

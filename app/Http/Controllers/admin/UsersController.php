<?php

namespace App\Http\Controllers\admin;

use App\Models\BankData;
use App\Models\CarOrder;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\MobileData;
use App\Models\PropertyOrder;
use App\Models\Transfer;
use App\Notifications\BookingRequestReceived;
use App\Notifications\FromAdmin;
use App\User;
use Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Notification;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::where('role',2)->get();

        return view('admin.users.index', compact('users'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        return view('admin.users.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'category_id' => 'required',
        ]);
        $user = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'category_id' => $request->category_id,
            'role' => 2,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/users')->withFlashMessage(json_encode(['success' => true, 'msg' => 'User Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->first();
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        $categories=Category::all();
        return view('admin.users.edit', compact('user','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',


        ]);
        $user = User::where('id', $id)->first();
        $user->update([
            'category_id' => $request->category_id,
        ]);
        if ($request->email != $user->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $user->update([
                'email' => $request->email
            ]);

        }
        if ($request->f_name != $user->f_name) {

            $user->update([
                'f_name' => $request->f_name
            ]);

        }
        if ($request->l_name != $user->l_name) {

            $user->update([
                'l_name' => $request->l_name
            ]);

        }

        if ($request->password != '') {
            $user->update([
                'password' => Hash::make($request->password),
            ]);
        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $user->photo = '/uploads/profiles/' . $imageName;
            $user->save();

        }


        return redirect('/webadmin/users' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'User Edited Successfully']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->delete();
//        $user->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'User Deleted Successsfully']));
    }
    public function sendemail(){
        $users=User::where('role',2)->get();
        return view('admin.users.send-email',compact('users'));


    }
    public function storeEmail(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'message' => 'required',


        ]);
        if(in_array('all',$request->user_id)){
            $users=User::where('role',2)->get();

        }else{
            $users=User::whereIn('id',$request->user_id)->get();

        }
        foreach ($users as $user){
            $data = [
                'subject' => 'From Admin',
                'msg' => $request->message,
            ];
            Mail::send('emails.admin-email', $data, function ($message) use ($user) {
                $message->from('blog@gmail.com', 'Blog')
                    ->to($user->email)
                    ->subject('From admin, Blog');
            });

        }
        return redirect()->back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الارسال بنجاح']));



    }


}

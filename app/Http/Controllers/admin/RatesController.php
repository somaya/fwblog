<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\User;
use Illuminate\Http\Request;

class RatesController extends Controller
{
    public function index($id)
    {
        $rates = Rate::where('user_id',$id)->get();
        $user=User::find($id);
        return view('admin.rates.index', compact('rates','user'));
    }
    public function create($id)
    {
        $user=User::find($id);
        return view('admin.rates.add',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $user=User::find($id);
        $request->validate([

            'degree' => 'required',

        ]);
        $user = Rate::create([
            'degree' => $request->degree,
            'comment' => $request->comment,
            'user_id' => $id,
            'admin_id' => auth()->id(),

        ]);


        return redirect('/webadmin/user/'.$id.'/rates')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Review Added Successfully']));
    }
}

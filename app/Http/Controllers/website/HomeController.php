<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Slider;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    public function getPosts()
    {

        $posts = Post::orderBy('created_at','desc')->get();
        return view('website.home', compact('posts'));
    }
    public function home()
    {

        return view('welcome');
    }
}
